import sympy
import random

def pierwiastek_pierwotny(g,n):
    lista = []
    f = 0
    i = 1
    x = g
    while True:
        x = x * g
        x = x % n
        i += 1
        lista.append(x)
        if x == g:
            break
    if i == n:
        lista.sort()
        for x in range(1, n):
            if x != lista[x-1]:
                f = 1
        if f == 0:
            return True
    else:
        return False


def dla_dwoch_uzytkownikow():
    print()
    print()
    print()
    print('==========DWÓCH UŻYTKOWNIKÓW==========')
    n = sympy.randprime(10000,100000)
    while True:
        g = random.randrange(10000, n)
        if pierwiastek_pierwotny(g, n):
            break
    print('n ' + str(n))
    print('g: ' + str(g))
    x = random.randrange(10000,100000)
    print('x: ' + str(x))
    X = pow(g,x) % n
    print('X: ' + str(X))
    y = random.randrange(10000,100000)
    print('y: ' + str(y))
    Y = pow(g,y) % n
    print('Y: ' + str(Y))

    A_k = pow(Y, x) % n
    B_k = pow(X, y) % n

    print('Klucz sesji: '+ str(A_k))
    print('Klucz sesji: '+ str(B_k))
def dla_grupy_uzytkownikow():
    print('==========GRUPA UŻYTKOWNIKÓW==========')
    n = sympy.randprime(10000, 100000)
    while True:
        g = random.randrange(10000, n)
        if pierwiastek_pierwotny(g, n):
            break
    print('Podaj liczbę użytkowników: ')
    liczba_uzytkownikow = input()
    print('n: ' + str(n))
    print('g: ' + str(g))
    klucze_uzytkownikow = []
    for i in range(int(liczba_uzytkownikow)):
        klucze_uzytkownikow.append(random.randrange(10000, 100000))
    k = pow(g, klucze_uzytkownikow[0]) % n
    for i in range(1,int(liczba_uzytkownikow)):
        print(klucze_uzytkownikow[i])
        k = pow(k, klucze_uzytkownikow[i]) % n
    print('Klucz sesji dla grupy użytkowników: ' + str(k))

dla_dwoch_uzytkownikow()
dla_grupy_uzytkownikow()